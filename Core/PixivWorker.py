from Utils.Utils import make_ua
from bs4 import BeautifulSoup
import requests
import random
import re

__all__ = ["PixivSession", "Pixiv_Req"]


# subclass by Http_Req
class PixivSession:
    def __init__(self):
        self.headers = {
            'User-Agent': make_ua(),
            'Accept-Charset': 'utf-8;q=0.7,*;q=0.7',
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
            'Connection': 'keep-alive'
        }
        self.data = {
            'pixiv_id': 'user_evv6297',
            'password': 'a1246578',
            'source': 'pc'
        }
        self.params = {
            'lang': 'zh',
            'source': 'pc',
            'view_type': 'page',
            'ref': 'wwwtop_accounts_index'
        }
        self.Session = requests.session()
        self.Session.headers = self.headers
        self.Connections = 0
        self.login_url = "https://accounts.pixiv.net/api/login?lang=zh"
        self.Log = False

    def Login(self):
        if self.Log:
            raise Exception("this Sessionmaker is Already Logged")

        # Let our session can access page that is after login
        session = self.Session
        params = self.params
        data = self.data
        login_url = self.login_url

        try:
            # Get token for login to Pixiv
            req_1 = session.get(url="https://accounts.pixiv.net/login", params=params)

            pattern = re.compile(r'name="post_key" value="(.*?)"')
            pattern_result = pattern.findall(req_1.text)
            data['post_key'] = pattern_result[0]

            # Login in
            rs = session.post(login_url, data=data)
        except requests.exceptions.RequestException as ex:
            pass
            # repack to remove path, domain, etc

        # Set our session to LoginReady
        self.Session = session
        self.Log = True
        return self.Session


class Pixiv_Req(PixivSession):
    def __init__(self):
        super().__init__()
        self.headers = {
            'User-Agent': make_ua(),
            'Accept-Charset': 'utf-8;q=0.7,*;q=0.7',
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
            'Connection': 'keep-alive'
        }
        self.uri = None

        self.PIXIV_VIEW_URL = "https://www.pixiv.net/member_illust.php?mode=medium&illust_id={}"

    def get_Pixiv(self, Pid, session, headers={}):
        def Metadata_Processing(response):
            bs = BeautifulSoup(response, "html.parser")

            # CSS Selector to get main Image_URL that we need
            # <div class=_illust_modal _hidden ui-modal-close-box>
            res = bs.select('div["class=_illust_modal _hidden ui-modal-close-box"] img')
            temp = res[0]['data-src']
            # Return an url of main_image
            return temp

        try:
            response = session.get(self.PIXIV_VIEW_URL.format(Pid), headers=headers)
        except requests.exceptions.RequestException as ex:
            raise Exception("Got an exception while Requests.get on Http_Req , Detail : {}".format(ex))

        if response.status_code == 200:
            return Metadata_Processing(response.text)
        else:
            raise Exception("Http_Req raise an Exception, Status-Code={}".format(response.status_code))

    def download_req(self, targetURL):

        def removebig(x):
            rmbig = re.compile('_big')
            x = re.sub(rmbig, "", x)
            return x.strip()

        self.headers['Referer'] = removebig(targetURL)

        return self.headers



