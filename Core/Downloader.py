from queue import PriorityQueue
from Utils import *
import requests
import os
import sys
import threading


class Downloader:
    @classmethod
    def initialize(cls, path, Threads=3, Session=requests.session()):
        cls.refresh_Event = threading.Event()
        cls.lock = threading.Lock()
        cls.queue = PriorityQueue()
        cls.task = 0
        cls.loop = True

        cls.url = ""
        cls.session = Session
        cls.path = os.getcwd() + path + "/"

        cls.headers = {
            'User-Agent': make_ua(),
            'Accept-Charset': 'utf-8;q=0.7,*;q=0.7',
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
            'Connection': 'keep-alive'
        }

        for i in range(0, Threads):
            print("Thread created! Number of threads: {}".format(i))
            cls.thread = threading.Thread(target=cls.download_thread)
            cls.thread.daemon = False
            cls.thread.start()

    @classmethod
    def download_thread(cls):
        while cls.loop:
            cls.refresh_Event.clear()
            if not cls.queue.empty():

                url, headers, task = cls.queue.get()

                filename = url.split('/')
                filename = filename[len(filename) - 1]

                if not headers:
                    r = cls.session.get(url, stream=True, headers=cls.headers)
                else:
                    r = cls.session.get(url, stream=True, headers=headers)

                with open(cls.path + filename, 'wb') as f:
                    for chunk in r.iter_content(chunk_size=1024 * 1024):
                        if chunk:  # filter out keep-alive new chunks
                            f.write(chunk)
                            f.flush() # clean up

                cls.task -= 1
                print("\nTask done! Task number: {}".format(task))
            else:
                # wait until we have something to do
                cls.refresh_Event.wait()

    @classmethod
    def new(cls, url, headers=None):
        if not url:
            raise Exception("No url called")

        cls.queue.put((url, headers, cls.task))
        print("\nNew Task submitted, Task ID: {}".format(cls.task))
        cls.task += 1
        cls.refresh_Event.set()

    @classmethod
    def Disable(cls):
        cls.loop = False
        cls.refresh_Event.set()


