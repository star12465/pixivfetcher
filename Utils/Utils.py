import random


def make_ua():
    rrange = lambda a, b, c=1: c == 1 and random.randrange(a, b) or int(
        1.0 * random.randrange(a * c, b * c) / c)
    return 'Mozilla/%d.0 (Windows NT %d.%d) AppleWebKit/%d (KHTML, like Gecko) Chrome/%d.%d Safari/%d' % (
        rrange(4, 7, 10), rrange(5, 7), rrange(0, 3), rrange(535, 538, 10),
        rrange(21, 27, 10), rrange(0, 9999, 10), rrange(535, 538, 10)
    )