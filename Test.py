from Core import *
from Utils import *
from bs4 import BeautifulSoup
import requests
import re

if __name__ == '__main__':
    headers = {
        'User-Agent': make_ua(),
        'Connection': 'keep-alive',
        "accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
        "accept-language": "en-US",
        "cache-control": "max-age=0",
        "dnt": "1",
        "upgrade-insecure-requests": "1",
        "referer": "https://www.pixiv.net/"
    }

    login_url = "https://accounts.pixiv.net/api/login?lang=zh"
    data = {
        'pixiv_id': 'user_evv6297',
        'password': 'a1246578',
        'source': 'pc'
    }
    params = {
        'lang': 'zh',
        'source': 'pc',
        'view_type': 'page',
        'ref': 'wwwtop_accounts_index'
    }
    s = requests.Session()
    s.header = headers

    try:
        # preprocess on login
        r = s.get(url="https://accounts.pixiv.net/login", params=params)
        pattern = re.compile(r'name="post_key" value="(.*?)"')
        pattern_result = pattern.findall(r.text)
        data['post_key'] = pattern_result[0]

        # login here
        rs = s.post(login_url, data=data, headers=headers)
        print(rs.text)
        print(rs.status_code)
    except requests.exceptions.RequestException as ex:
        pass
        # repack to remove path, domain, etc

    def processing_metadata(res):
        bs = BeautifulSoup(res, "html.parser")
        res = bs.select('div["class=_illust_modal _hidden ui-modal-close-box"] img')
        print(res[0]['data-src'])

    hr_res = s.get("https://www.pixiv.net/member_illust.php?mode=medium&illust_id=57102356")
    #print(hr_res.text)
    processing_metadata(hr_res.text)

