int = 0
float = 0.1
str = "abcdefg1234560"
char = 'a'
bool = True # or False
dict = {"kw": "data", "kw2": "data2"}
array = [0, 1, 2, 3, 4, 5]
tuple = (1, 2, 3, 4)

a = 1


def getForce(m, a):
    return m*a


force = getForce(1, 2)  # call function
print(force)
